/**
 *
 * @module fun-generator
 */
;(function () {
  'use strict'

  /* imports */
  const { append, fold, map, repeat } = require('fun-array')
  const { curry } = require('fun-function')
  const { map: omap, ap: oap, toPairs, ofPairs } = require('fun-object')
  const sample = require('fun-sample')
  const t = require('fun-type')
  const { inputs } = require('guarded')
  const st = require('monad-state')

  const row = (i, m) => m.map((column) => column[i])
  const transpose = m => m[0].map((v, i) => row(i, m))

  /**
   *
   * fun :: (a -> s) -> ((s, s) -> s) -> (s -> [s, s]) -> (s -> [b, s]) -> s ->
   *   [a -> b, s]
   *
   * @function module:fun-generator.fun
   *
   * @param {Function} a2s - a -> s
   * @param {Function} ss2s - (s, s) -> s
   * @param {Function} s2ss - s -> [s, s]
   * @param {Function} s2sb - s -> [b, s]
   * @param {*} s - seed
   *
   * @return {Array} [a -> b, s]
   */ // eslint-disable-next-line max-params
  const fun = (a2s, ss2s, s2ss, s2sb, s) => {
    const [si, sF] = s2ss(s)
    const a2b = a => st.run(ss2s(a2s(a), si), s2sb)

    return [a2b, sF]
  }

  /**
   *
   * objectOf :: (s -> [[string], s]) -> (s -> [b, s]) -> s ->
   *   [{ string: b }, s]
   *
   * @function module:fun-generator.objectOf
   *
   * @param {Function} keyGen - s -> [[string], s]
   * @param {Function} valueGen - s -> [b, s]
   * @param {*} s0 - seed
   *
   * @return {Array} [{ string: b }, s]
   */
  const objectOf = (keyGen, valueGen, s0) => {
    const [keys, s1] = keyGen(s0)
    const [values, s2] = tuple(repeat(keys.length, valueGen), s1)

    return [ofPairs(transpose([keys, values])), s2]
  }

  /**
   *
   * record :: { ka: s -> [a, s], ..., kn: s -> [n, s] } -> s ->
   *   [{ ka: a, ..., kn: n }, s]
   *
   * @function module:fun-generator.record
   *
   * @param {Object} gs - { ka: s -> [a, s], ..., kn: s -> [n, s] }
   * @param {*} s - seed
   *
   * @return {Array} [{ ka: a, ..., kn: n }, s]
   */
  const record = (gs, s) => st.map(
    ofPairs,
    x => tuple(map(([k, v]) => st.map(x => [k, x], v), toPairs(gs)), x),
    s
  )

  /**
   *
   * arrayOf :: (s -> [Z+, s]) -> (s -> [a, s]) -> s -> [[a], s]
   *
   * @function module:fun-generator.arrayOf
   *
   * @param {Function} lengthGen - s -> [Z+, s]
   * @param {Function} g - s -> [a, s]
   * @param {*} s0 - seed
   *
   * @return {Array} [[a], s]
   */
  const arrayOf = (lengthGen, g, s0) => {
    const [length, s1] = lengthGen(s0)

    return tuple(repeat(length, g), s1)
  }

  /**
   *
   * tuple :: [(s -> [a, s]), ..., (s -> [n, s])] -> s -> [[a, ..., n], s]
   *
   * @function module:fun-generator.tuple
   *
   * @param {Array<Function>} gs - [(s -> [a, s]), ..., (s -> [n, s])]
   * @param {*} s0 - seed
   *
   * @return {Array} [[a, ..., n], s]
   */
  const tuple = (gs, s0) =>
    fold(([ys, s], g) => st.map(v => append(v, ys), g, s), [[], s0], gs)

  /**
   *
   * vectorOf :: Z+ -> (s -> [a, s]) -> s -> [[a], s]
   *
   * @function module:fun-generator.vectorOf
   *
   * @param {Number} n - length of vector
   * @param {Function} g - s -> [a, s]
   * @param {*} s0 - seed
   *
   * @return {Array} [[a], s]
   */
  const vectorOf = (n, g, s0) => tuple(repeat(n, g), s0)

  /**
   *
   * string :: (s -> [Z+, s]) -> (s -> [char, s]) -> s -> [string, s]
   *
   * @function module:fun-generator.string
   *
   * @param {Function} lengthGen - s -> [Z+, s]
   * @param {Function} charGen - s -> [char, s]
   * @param {*} s - seed
   *
   * @return {Array} [string, s]
   */
  const string = (lengthGen, charGen, s) =>
    st.map(s => s.join(''), s => arrayOf(lengthGen, charGen, s), s)

  /**
   *
   * char :: string -> (s -> [p, s]) -> s -> [char, s]
   *
   * @function module:fun-generator.char
   *
   * @param {String} string - to get character from
   * @param {Function} prng - s -> [p, s] | p <- [0, 1)
   * @param {*} s - seed
   *
   * @return {Array} [char, s]
   */
  const char = (string, prng, s) =>
    st.map(sample.member(string.split('')), prng, s)

  /**
   *
   * bool :: (s -> [p, s]) -> s -> [bool, s]
   *
   * @function module:fun-generator.bool
   *
   * @param {Function} prng - s -> [p, s] | p <- [0, 1)
   * @param {*} s - seed
   *
   * @return {Array} [bool, s]
   */
  const bool = (prng, s) => st.map(sample.member([false, true]), prng, s)

  /**
   *
   * int :: int -> int -> (s -> [p, s]) -> s -> [int, s]
   *
   * @function module:fun-generator.int
   *
   * @param {Number} min - lower bound
   * @param {Number} max - upper bound
   * @param {Function} prng - s -> [p, s] | p <- [0, 1)
   * @param {*} s - seed
   *
   * @return {Array} [int, s]
   */
  const int = (min, max, prng, s) => st.map(sample.integer(min, max), prng, s)

  /**
   *
   * member :: [a, ...n] -> (s -> [p, s]) -> s -> [(x | x <- [a, ...n]), s]
   *
   * @function module:fun-generator.member
   *
   * @param {Array} set - to pick from
   * @param {Function} prng - s -> [p, s] | p <- [0, 1)
   * @param {*} s - seed
   *
   * @return {Array} [(x | x <- set), s]
   */
  const member = (set, prng, s) => st.map(sample.member(set), prng, s)

  /**
   *
   * num :: num -> num -> (prng - s -> [p, s]) -> s -> [num, s]
   *
   * @function module:fun-generator.num
   *
   * @param {Number} min - lower bound
   * @param {Number} max - upper bound
   * @param {Function} prng - s -> [p, s] | p <- [0, 1)
   * @param {*} s - seed
   *
   * @return {Array} [(x | x <- [min, max)), s]
   */
  const num = (min, max, prng, s) => st.map(sample.number(min, max), prng, s)

  const guards = omap(inputs, {
    fun: t.tuple([t.fun, t.fun, t.fun, t.fun, t.any]),
    objectOf: t.tuple([t.fun, t.fun, t.any]),
    record: t.tuple([t.objectOf(t.fun), t.any]),
    arrayOf: t.tuple([t.fun, t.fun, t.any]),
    tuple: t.tuple([t.arrayOf(t.fun), t.any]),
    vectorOf: t.tuple([t.num, t.fun, t.any]),
    string: t.tuple([t.fun, t.fun, t.any]),
    char: t.tuple([t.string, t.fun, t.any]),
    bool: t.tuple([t.fun, t.any]),
    int: t.tuple([t.num, t.num, t.fun, t.any]),
    member: t.tuple([t.array, t.fun, t.any]),
    num: t.tuple([t.num, t.num, t.fun, t.any])
  })

  const api = { num, fun, objectOf, record, arrayOf, tuple, bool, string, char,
    int, member, vectorOf }

  /* exports */
  module.exports = omap(curry, oap(guards, api))
})()

