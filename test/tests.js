;(function () {
  'use strict'

  /* imports */
  const { concat, map } = require('fun-array')
  const arrange = require('fun-arrange')
  const { compose } = require('fun-function')
  const object = require('fun-object')
  const { equalDeep: equal, and } = require('fun-predicate')
  const sample = require('fun-sample')
  const { gte, lte } = require('fun-scalar')
  const funTest = require('fun-test')
  const { arrayOf, string, bool, member, num, objectOf, tuple, fun, record,
    vectorOf } = require('fun-type')

  const fprng = s => [Math.random(), s + 1]
  const char = and(string, x => x.length === 1)

  const aSet = ['a', 1, true]

  const propertyTests = map(
    x => x,
    [
    ]
  )

  const prob = and(num, and(gte(0), lte(1)))
  const r1 = {
    a: fprng,
    b: fprng
  }
  const isR1 = record({ a: prob, b: prob })
  const keyGen = s => [['a', 'b', 'c'], s + 1]
  const alphabet = 'abcdefghijklmnopqrstuvwxyz'
  const charGen = s => [alphabet[s % alphabet.length - 1], s + 1]
  const lengthGen = s => [3, s]

  const subst = s => alphabet.includes(s)
  const a2s = a => sample.integer(0, Math.pow(2, 32), (a + 50) / 101)
  const ss2s = (a, b) => Math.floor((a + b) % Math.pow(2, 32))
  const s2ss = s => [s, s + 1]
  const s2sb = s => sample.member(alphabet.split(''), (s / Math.pow(2, 32)))

  const tests = map(
    compose(
      object.ap({ contra: object.get }),
      arrange({ inputs: 0, predicate: 1, contra: 2 })
    ), [
      [[a2s, ss2s, s2ss, s2sb, 0], ([f]) => char(f(0)), 'fun'],
      [[a2s, ss2s, s2ss, s2sb, 0], tuple([fun, equal(1)]), 'fun'],
      [[keyGen, fprng, 0], tuple([objectOf(prob), equal(4)]), 'objectOf'],
      [[r1, 0], tuple([isR1, equal(2)]), 'record'],
      [
        [lengthGen, fprng, 0],
        tuple([arrayOf(prob), equal(3)]),
        'arrayOf'
      ],
      [[[fprng, fprng], 0], tuple([tuple([prob, prob]), equal(2)]), 'tuple'],
      [[3, fprng, 0], tuple([vectorOf(3, prob), equal(3)]), 'vectorOf'],
      [['abc', fprng, 0], tuple([char, equal(1)]), 'char'],
      [[fprng, 0], tuple([bool, equal(1)]), 'bool'],
      [[-10, 10, fprng, 0], tuple([and(gte(-10), lte(10)), equal(1)]), 'num'],
      [[-10, 10, fprng, 0], tuple([and(gte(-10), lte(10)), equal(1)]), 'int'],
      [[aSet, fprng, 0], tuple([member(aSet), equal(1)]), 'member'],
      [[lengthGen, charGen, 0], tuple([subst, equal(3)]), 'string']
    ])

  /* exports */
  module.exports = concat(tests, propertyTests).map(x => funTest.sync(x))
})()

